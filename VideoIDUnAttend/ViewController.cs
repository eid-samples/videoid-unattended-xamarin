﻿using System;
using CoreGraphics;
using UIKit;
using VideoID.Framework.iOS;

namespace VideoIDUnAttend
{
    public partial class ViewController : UIViewController, IVideoIDDelegate
    {
        protected ViewController(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Perform any additional setup after loading the view, typically from a nib.

            UIButton button = new UIButton();
            CGRect ScreenBounds = UIScreen.MainScreen.Bounds;
            button.Frame = new CGRect(10f, 50f, 150f, 50f);
            button.SetTitle("Authenticate", UIControlState.Normal);
            button.BackgroundColor = UIColor.Green;

            VideoID.Framework.iOS.EidStyles.Roi.LinesColorSuccess = UIColor.Blue;
            VideoID.Framework.iOS.EidStyles.Tick.LinesColor = UIColor.Blue;
            VideoID.Framework.iOS.EidStyles.Notification.BackgroundColor = UIColor.Red;
            VideoID.Framework.iOS.EidStyles.MultimediaNotification.MessageColor = UIColor.Red;
            VideoID.Framework.iOS.EidStyles.Detection.FaceLineWidth = 5;



            button.TouchUpInside += (object sender, System.EventArgs e) => {
                var controller = new VideoIDViewController("https://etrust-sandbox.electronicid.eu/v2", "Show documentation to get auth tokens", "es", 62);
                controller.Delegate = this;
                //PresentViewController(controller, true, null);
                ShowViewController(controller, this);
            };
            View.AddSubview(button);
        }

        public void OnCompleteWithVideoID(string videoID)
        {
            // Create Alert
            var okAlertController = UIAlertController.Create("Success", videoID, UIAlertControllerStyle.Alert);

            // Add Action
            okAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));

            // Present Alert
            PresentViewController(okAlertController, true, null);
        }

        public void OnErrorWithCode(string code, string message)
        {
            // Create Alert
            var okAlertController = UIAlertController.Create(code, message, UIAlertControllerStyle.Alert);

            // Add Action
            okAlertController.AddAction(UIAlertAction.Create("OK", UIAlertActionStyle.Default, null));

            // Present Alert
            PresentViewController(okAlertController, true, null);
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}
